import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm, NgModel, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user';
import { SignupComponent } from '../signup/signup.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  errConnexion!: boolean;

  constructor( private router: Router, private formBuilder: FormBuilder,  public dialog: MatDialog, private authService: AuthService) { }

  authForm!: FormGroup;
  isSubmitted  =  false;

  pseudo!: string;
  phone!: string;
  password!: string;

  token !: string;

  loading = false;

  message_error !: string;

  formLogin: any = {
    username: null,
    password: null
  };

  showTokenInput = false;


  ngOnInit(): void {
    this.authForm  =  this.formBuilder.group({
      pseudo: ['', Validators.required],
      phone: ['', Validators.required],
      password: ['', Validators.required],
      token:['']
  });
  }

  async signIn(f: NgForm){
   console.log(f.value);
   console.log(f.valid);
    
   if (f.valid) {
    this.loading = true;
    await new Promise(f => setTimeout(f, 4000));
    this.loading = false;
    this.errConnexion =true;
   }
  }

  login(): void {
    console.log(this.pseudo);

    if (this.token != null) {
      this.authService.loginValid(this.pseudo,this.phone, this.password, this.token).subscribe(
        data => {
          console.log(data);
        }, err => {
          console.log(err.error);
          this.message_error = err.error;
        }
      )
    }
    else {
      this.authService.login(this.pseudo,this.phone, this.password).subscribe(
        data => {
          console.log(data);
        }, err => {
          console.log(err.error);
          this.message_error = err.error;
        }
      )    
      if (this.message_error = "Veuillez écrire le code reçu par SMS.") {
        this.showTokenInput = true;
      }
    }
  }

  openRegister() {
    console.log("Open register");
    this.dialog.closeAll();
    this.dialog.open(SignupComponent, {
      width:'300px'
    });
  }
  

}
