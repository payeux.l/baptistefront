import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  authForm!: FormGroup;
  loading = false;

  errConnexion!:boolean;

  validMdp=false;

  pseudo!: string;
  password!: string;
  password_confirm!: string;
  phone!:string;
  token!:string;

  form2: any = {
    username: null,
    phone: null,
    password: null
  };

  validSend = false;

  errorMessage = '';

  constructor(private router: Router,private formBuilder: FormBuilder, private authService: AuthService, public dialog: MatDialog) { }

  

  ngOnInit(): void {
    this.authForm  =  this.formBuilder.group({
      pseudo: ['', Validators.required],
      phone: ['', Validators.required],
      password: ['', Validators.required],
      password_confirm: ['', Validators.required],
      
    });

    if (this.password == this.password_confirm) {
      this.validMdp = true;
    }
    else if (this.password != this.password_confirm) {
      this.validMdp = false;
    }

  }

  /*

  signIn(form: NgForm): void {
    console.log(form.value);
    console.log(form.valid);

    console.log(form.value.pseudo);

    this.pseudo = form.value.pseudo;
    this.phone = form.value.phone;
    this.password = form.value.password;
    this.password_confirm = form.value.password_confirm;

    if (this.password == this.password_confirm) {
      this.validMdp = true;
    }
    else if (this.password != this.password_confirm) {
      this.validMdp = false;
    }
     
    if (form.valid && this.validMdp) {
      const { pseudo, phone, password } = this.form2;
     this.loading = true;
     this.authService.register(pseudo, phone, password).subscribe(
      data => {
        this.validSend = true;
        console.log(data);
      },
      err => {
        this.errorMessage = err.error.message;
      }
     );
     
     this.loading = false;
     this.errConnexion =true;
    }
   }
   */

   signIn(): void {
    
    if (this.password == this.password_confirm) {
      this.validMdp = true;
    }
    else if (this.password != this.password_confirm) {
      this.validMdp = false;
    }
    
    if (this.validMdp) {
      console.log (this.pseudo, this.phone, this.password);
      this.authService.register(this.pseudo, this.phone, this.password).subscribe(
        data => {
          console.log(data);
          this.validSend = true;
          console.log(data.error);
  
        }, err => {
          this.errorMessage= err.error;
          this.validSend = false;
          console.log(err.error);
        });
    }
    

    
  }

  openLogin() {
    console.log("Open login modal");
    this.dialog.closeAll();
    this.dialog.open(LoginComponent, {
      width:'300px'
    });
  }


  closeModal(): void {
    this.dialog.closeAll();
  }

}
