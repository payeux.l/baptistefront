import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignupRoutingModule } from './signup-routing.module';
import { SignupComponent } from './signup.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from 'src/app/services/auth.service';


@NgModule({
  declarations: [
    SignupComponent
  ],
  providers: [
    AuthService
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    SignupRoutingModule,
    HttpClientModule
  ]
})
export class SignupModule { }
