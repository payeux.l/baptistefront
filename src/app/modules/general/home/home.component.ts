import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { questionnaireStructure } from 'src/app/models/questionnaireStructure';
import { LoginComponent } from '../login/login.component';
import { SignupComponent } from '../signup/signup.component';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  questForm!: FormGroup;
  questFormSuite!: FormGroup;
  submitted = false;

  repOui!: boolean;

  repNon!: boolean;

  repOuiSuite!:boolean;
  repNonSuite!:boolean;

  showAll!: boolean;

  number_v!: number;

  questionnaire: any[] = [
    {
      "question": "Comment vas-tu ?", "reponseOui": "Bien", "reponseNon": "Mal", "resultat": "caché"
    },
    {
      "question": "Es-tu majeur ?", "reponseOui": "Oui", "reponseNon": "Non", "resultat": "caché"
    },
  ]

  reponses: questionnaireStructure[] = [];

  constructor(private formBuilder: FormBuilder, public dialog: MatDialog) { }

  ngOnInit(): void {

    this.number_v = 1; //valeur du compteur

    this.questForm = this.formBuilder.group({
      RepOui: [false],
      RepNon: [false],
    });

    this.questFormSuite = this.formBuilder.group({
      RepOuiSuite: [false],
      RepNonSuite: [false],
    })
    
    
    
  }
  get f() { return this.questForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.questForm.invalid) {
        return;
    }
}



onReset() {
    this.submitted = false;
    this.questForm.reset();
}

openLogin() {
  console.log("openModal for login");
  this.dialog.open(LoginComponent, {
    width:'300px'
  });

}


}
