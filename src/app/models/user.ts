export interface User {
    id: number;
    pseudo: string;
    password: string;
    secret: string;
    validated: boolean;
}
