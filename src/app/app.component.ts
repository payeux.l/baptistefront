import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Point de Rupture';
  version = 'Angular version 14.1.1';


  imageSource =1; //https://picsum.photos/1920/1195 img aleatoire

  changeImage() {
    this.imageSource++;

    if (this.imageSource == 10) {
      this.imageSource =1;
    }
  }
}
