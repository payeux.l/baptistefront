import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../models/user';
import { ApiService } from './api.service';

const AUTH_API = 'http://localhost:9000/api/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  public signIn(userData: User){
    localStorage.setItem('ACCESS_TOKEN', "access_token");
  }  
  
  public isLoggedIn(){
    return localStorage.getItem('ACCESS_TOKEN') !== null;  }  
    
  public logout(){
    localStorage.removeItem('ACCESS_TOKEN');
  }

  /*
  post(user: User): Promise<User> {
    return new Promise((resolve, reject) => {
      this.apiService.postRequest('/user/' + this.user.properties.id, User.GetModelForAPI(user))
        .subscribe(
          res => resolve(User.ParseFromObject(res)),
          err => reject(<any>err));
    });
  }*/

  register(pseudo: string, phone: string, password: string): Observable<any> {
    return this.http.post(AUTH_API + 'user', {
      pseudo,
      phone,
      password
    }, httpOptions);
  }

  login(pseudo: string, phone: string, password: string): Observable<any> {
    return this.http.post(AUTH_API + 'login', {
      pseudo,
      phone,
      password
    }, httpOptions);
  }

  loginValid(pseudo: string, phone: string, password: string, token: string): Observable<any> {
    return this.http.put(AUTH_API + 'valid', {
      pseudo,
      phone,
      password,
      token
    }, httpOptions);
  }

}
